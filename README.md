# Alpine-Dropbear based SSH/SFTP Server

This quick project tries to provide a quick SSH/SFTP non-root server ready for transfering files.

By deault a new user will be created. Please review Makefile to configure the arguments passed during image building. Also remember that image will be created locally (it is just a quick sftp server to use for yourself).

A public SSH Key is required (mykey.pub) to allow access to the SSH/SFT Server as defined user (sshsrv by default, with UID 10000). You can use make key to create this key or use your own (make sure you use the right key file name).

Volumes can be provided to overwrite authorization or even home for file transfers. Always take care or USERIDs used in case of volumes as they should be equal. Be aware of permissions for your files.

## Quick Usage:
- make all -- This will generate a key (mykey), build the image sftp-server:latest with the generated key and run the SSH/SFTP Server on a dynamic port. 

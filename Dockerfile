FROM alpine
RUN apk add --update \
dropbear \
dropbear-ssh \
openssh-sftp-server
RUN mkdir -p /etc/dropbear \
&& dropbearkey -t dss -f /etc/dropbear/dropbear_dss_host_key \
&& dropbearkey -t rsa -f /etc/dropbear/dropbear_rsa_host_key

ARG username
ENV USERNAME $username
ARG userid
ENV USERID $userid

RUN addgroup -S ${USERNAME} -g ${USERID} && \
    adduser -s /bin/sh -S -D -G ${USERNAME} -u ${USERID} ${USERNAME}

WORKDIR /home/${USERNAME}

RUN addgroup ${USERNAME} shadow && \
    mkdir -p /home/${USERNAME}/.ssh && \
    chown -R ${USERNAME}:${USERNAME} /home/${USERNAME} && \
    mkdir -p /etc/dropbear && \
    chown -R ${USERNAME}:${USERNAME} /etc/dropbear && \
    chmod 700 /etc/dropbear && \
    chown root:${USERNAME} /etc/shadow*

COPY mykey.pub /home/${USERNAME}/.ssh/authorized_keys

RUN chown -R ${USERNAME}:${USERNAME} /home/${USERNAME}

EXPOSE 2222

USER ${USERID}

CMD dropbear -F -E -p 2222

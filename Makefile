all:
	make key build run
build:
	docker build --build-arg userid=10000 --build-arg username=sshsrv -t sftp-server --no-cache .
run: 
	(docker rm -fv sftp || true ) && docker run --name sftp -P -d sftp-server
	docker port sftp
key:
	ssh-keygen -t rsa -f mykey -N ""
